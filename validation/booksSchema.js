const joi = require('@hapi/joi')


const bookSchema = joi.object().keys({
    name: joi.string().min(1).required().messages({
        "any.required": `Mohon isi nama buku`
    }),
    year: joi.number().required().messages({
        "any.required": `Mohon isi year buku`
    }),
    author: joi.string().required().messages({
        "any.required": `Mohon isi author buku`
    }),
    summary: joi.string().required().messages({
        "any.required": `Mohon isi summary buku`
    }),
    publisher: joi.string().required().messages({
        "any.required": `Mohon isi publisher buku`
    }),
    pageCount: joi.number().required().messages({
        "any.required": `Mohon isi pageCount buku`
    }),
    readPage: joi.number().required().messages({
        "any.required": `Mohon isi readPage buku`
    }),
    reading: joi.boolean().required().messages({
        "any.required": `Mohon isi reading buku`
    }),
});

const validation = (request, h) => {

}

module.exports = {
    bookSchema, validation
}