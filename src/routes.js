const { options } = require('joi');
const { bookSchema, handleError, validation } = require('../validation/booksSchema.js');
const { addBooks, getAllBooks, getBookbyID, updateBookbyId, deleteBookById, homeStart } = require('./handler.js')


const routes = [
    {
        method: 'GET',
        path: '/',
        handler: homeStart
    },
    {
        method: 'POST',
        path: '/books',
        handler: addBooks,
    },
    {
        method: 'GET',
        path: '/books',
        handler: getAllBooks
    },
    {
        method: 'GET',
        path: '/books/{id}',
        handler: getBookbyID
    },
    {
        method: 'PUT',
        path: '/books/{id}',
        handler: updateBookbyId,
    },
    {
        method: 'DELETE',
        path: '/books/{id}',
        handler: deleteBookById
    }
];

module.exports = routes;