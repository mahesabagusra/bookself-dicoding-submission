const { nanoid } = require('nanoid');
const HapiUrl = require('hapi-url')
const books = require('./books');
const { bookSchema } = require('../validation/booksSchema');

const homeStart = (request, h) => {
    const url = HapiUrl;

    const response = h.response({
        status: 'Success',
        message: `Server Started at : ${url.current(request)}`
    });
    response.code(200);
    return response;
}

const addBooks = (request, h) => {
    const { name, year, author, summary, publisher, pageCount, readPage, reading } = request.payload;
    const id = nanoid(16);
    const insertedAt = new Date().toISOString();
    const updatedAt = insertedAt
    let finished = false;

    //validate payload
    const { error, value } = bookSchema.validate(request.payload, { abortEarly: false });

    if (pageCount === readPage) {
        finished = true;
    };

    if (readPage > pageCount) {
        const response = h.response({
            status: 'fail',
            message: "Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount"
        })

        response.code(400);
        return response
    };

    if (error) {
        const response = h.response({
            status: 'fail',
            message: `Gagal menambahkan buku. ${error.message}`
        })

        response.code(400);
        return response
    }

    const newBooks = {
        id, name, year, author, summary, publisher, pageCount, readPage, finished, reading, insertedAt, updatedAt
    }
    books.push(newBooks);
    const isSuccess = books.filter((book) => book.id === id).length > 0;

    if (isSuccess) {
        const response = h.response({
            status: 'success',
            message: 'Buku berhasil ditambahkan',
            data: {
                bookId: id,
                Body: { name, year, author, summary, publisher, pageCount, readPage, finished, reading, insertedAt, updatedAt }
            }
        });

        response.code(201);
        return response
    }
};

const getAllBooks = (request, h) => {
    const { name, finished, reading } = request.query;
    const finishedBooks = books.filter((book) => book.finished == finished).map(book => {
        return {
            id: book.id,
            name: book.name,
            publisher: book.publisher
        }
    });

    //error *TypeError: Cannot read property 'toLowerCase' of undefined* sehingga tidak membuat Optional terakhir. di postman berhasil tetapi di terminal error.
    const nameBooks = books.filter((book) => book.name.includes(name)).map(book => {
        return {
            id: book.id,
            name: book.name,
            publisher: book.publisher
        }
    });

    const readingBooks = books.filter((book) => book.reading == reading).map(book => {
        return {
            id: book.id,
            name: book.name,
            publisher: book.publisher
        }
    });

    const data = books.map((book) => {
        return {
            id: book.id,
            name: book.name,
            publisher: book.publisher
        }
    })

    const result = finished ? finishedBooks
        : reading ? readingBooks
            : name ? nameBooks
                : data

    const response = h.response({
        status: 'success',
        message: 'Data berhasil di ditampilkan',
        data: {
            books: result
        }


    });
    response.code(200)
    return response
}

const getBookbyID = (request, h) => {
    const { id } = request.params;

    const book = books.filter(book => book.id === id)[0];

    if (book !== undefined) {
        const response = h.response({
            status: 'success',
            message: 'Data berhasil di ditampilkan',
            data: {
                book
            }
        });

        response.code(200);
        return response
    }

    const response = h.response({
        status: 'fail',
        message: 'Buku tidak ditemukan'
    })

    response.code(404)
    return response
}

const updateBookbyId = (request, h) => {
    const { id } = request.params;
    const { name, year, author, summary, publisher, pageCount, readPage, reading } = request.payload;
    const updatedAt = new Date().toISOString();

    const index = books.findIndex((book) => book.id === id);
    const { error, value } = bookSchema.validate(request.payload, { abortEarly: false })

    if (readPage > pageCount) {
        const response = h.response({
            status: 'fail',
            message: 'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount'
        })

        response.code(400)
        return response
    }

    if (error) {
        const response = h.response({
            status: 'fail',
            message: `Gagal memperbarui buku. ${error.message}`
        });

        response.code(400)
        return response
    }

    if (index !== -1) {
        books[index] = {
            ...books[index],
            name,
            year,
            author,
            summary,
            publisher,
            pageCount,
            readPage,
            reading,
            updatedAt,
        }

        const response = h.response({
            status: 'success',
            message: 'Buku berhasil diperbarui',

        })
        response.code(200);
        return response
    }

    const response = h.response({
        status: 'fail',
        message: 'Gagal memperbarui buku. Id tidak ditemukan'
    });

    response.code(404);
    return response
};

const deleteBookById = (request, h) => {
    const { id } = request.params;

    const index = books.findIndex(book => book.id === id);

    if (index !== -1) {
        books.splice(index, 1);
        const response = h.response({
            status: 'success',
            message: 'Buku berhasil dihapus'
        });

        response.code(200);
        return response
    }

    const response = h.response({
        status: 'fail',
        message: 'Buku gagal dihapus. Id tidak ditemukan'
    });
    response.code(404);
    return response

};


module.exports = {
    addBooks, getAllBooks, getBookbyID, updateBookbyId, deleteBookById, homeStart
}